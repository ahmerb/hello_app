Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Setting the root route (listing 1.10)
  root 'application#goodbye'
end
